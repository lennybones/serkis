* started in OpenShift with a jekyll instance (closest to gollum prob)
* did `git clone ssh://536370ab5973ca9889000188@serkis-lbones.rhcloud.com/~/git/serkis.git/`
* did `cd ~/git/serkis`
* `rm -rf` all the jekyll stuff
* replaced with some markdown for gollum
* echo "i'm markdown, technically" > Home.md

But here's where i start to realize at least one possible problem. I hadn't noticed before, but when you run `gollum serve` on a gollum folder, it doesn't actually *generate* html. It must be doing it on the fly. That is, something like `jekyll serve` will grab up all the markdown in a jekyll instance and actually convert it to the html that gets served when requested. Gollum doesn't appear to do that. What that means is that if you want to serve gollum's html content (the stuff it renders out of raw markdown) you must have a gollum server *running* in order to do that conversion upon request. So that's going to be part of the problem I suspect. How that might or might not translate into "valid git repo" errors I'm not sure. I suspect, actually, that "valid git repo" is just a generic error that gets thrown when openshift is asked to do something it doesn't really understand. Imma test.

What I would actually expect to happen is what just happened to me when I did `git push`:

	remote: Stopping jekyll cart
	remote: Sending SIGTERM to jekyll:327650 ...
	remote: Building git ref 'master', commit 683dc2a
	remote: Preparing build for deployment
	remote: Deployment id is 004fccb5
	remote: Activating deployment
	remote: Starting jekyll cart
	remote: Executing bundle install
	remote: /var/lib/openshift/536370ab5973ca9889000188/app-root/runtime/repo/Gemfile not found
	remote: -------------------------
	remote: Git Post-Receive Result: failure
	remote: Activation status: failure
	remote: Activation failed for the following gears:
	remote: 536370ab5973ca9889000188 (Error activating gear: CLIENT_ERROR: Failed to execute: 'control start' for /var/lib/openshift/536370ab5973ca9889000188/jekyll)
	remote: Deployment completed with status: failure
	remote: postreceive failed
	To ssh://536370ab5973ca9889000188@serkis-lbones.rhcloud.com/~/git/serkis.git/
	   0cdaf7f..683dc2a  master -> master
	   
Notice the Gemfile and all the "jekyll" references. OpenShift simply is not seeing what it expects to see in a jekyll instance (becuase our repo no longer is). Why this would translate to "not a valid git repo" error I still don't know, but my executive summary (which isn't supposed to go at the bottom of a big, long tome, but fuck it) is: **OpenShift don't do gollum**.