

Git's actually super powerful. Lots of complicated ways to interact with it that I can't say I even recommend (or understand!). The specifics of how it works you probably know (i.e. u have a head from which you `checkout` or `branch` and then you work on those files as a separate, uncommitted instance of the code, then `add` them back to the staging area and prolly `commit` them to the head – let me know if u wanna go over more of that). But `push` and `pull` are obviously important.

#### push: remotes
Push is pretty straight forward in most cases. Your git repo probably has a remote defined. You can see it in two main ways:

1. from anywhere inside of your repo's folder structure, run `git remote -v` and you'll see something like:

		origin	https://lennybones@bitbucket.org/lennybones/serkis.git (fetch)
		origin	https://lennybones@bitbucket.org/lennybones/serkis.git (push)
	* it's normal for both fetch and push to be the same
	* notice it says "fetch" and not "pull" -- more on that momentarily
2. browse to your repo's folder and then into the .git folder
	1. this folder may not be visible using Finder, so using Terminal, you would...
		* ```cd ~/git/gollum/.git  # or wherever, but NOTE the dot!```
		* inside that folder is a plain file named "config" and in that file is a block something like this:
	
				[remote "origin"]
			        
				fetch = +refs/heads/*:refs/remotes/origin/*

				url = https://lennybones@bitbucket.org/lennybones/serkis.git

#### push origin master	
2. Okay, those are your "origin" remotes (where "origin" is the conventional/default name). What do we do with them? We send them code!
3. Basically this means that when you run `git push` (which is usually an alias, of sorts, for `git push origin master`) you are uploading the current head of the "master" branch of your code to the "origin" url (in this example, https://lennybones@bitbucket.org/lennybones/serkis.git). Note that any changes that haven't been explicitly committed aren't going to be sent. In fact, that's why gollum itself behaves the way it does: it *only* reads the committed code from the "master" branch of a repo. We've both been frustrated by that cycle where we make a change but then forget to commit and/or forget to restart gollum. A similar disappointment would happen if you forgot to `git commit` before you did `git push` to send your changes to GitHub (or BitBucket or whatever) -- your new (uncommitted) stuff would not appear there. So when you `git push` you're only uploading the committed head of the master unless you specify otherwise: e.g. one day might start doing things like `git push lennyslaptop mybranch` and that would push the currently-committed "mybranch" branch to some other url, in this case the one associated with "lennyslaptop." But that's not necessary to worry about right now.

#### pull
1. pull is a little more complex because a `git pull` is actually a `git fetch` coupled with a `git merge`. Additionally obscured is the fact that just typing `git pull` means it's gonna default to requesting the "master" branch from the "origin" location. Which is fine, usually what u want anyway. e.g. What's actually being run (most likely) when you type `git pull` is `git fetch origin master` and then `git merge`. What does this mean, though? It not only fetches new stuff from the "master" branch as it sits on the server identied by "origin," it tries to auto-merge the changes that it finds between the fetched code and what you have locally. That doesn't always work if you have a bunch of edited files sitting around whose edits conflict with the stuff that got fetched from remote. You'll be asked to fix the differences by hand.

#### replacing a repo with another
Think of this more or less the same as if you wanted to replace a single file with another. Let's say you have repo MyRepo with a remote of https://me@bitbucket.org/me/MyRepo.git. If you wanna replace just the files in your repo (and not have it point to an entirely new origin, e.g.), you remove all of the current files (keep the hidden files -- keep .git and .gitgnore, etc.) and replace them with your good stuff. Then add your new files, then do your add and commit and push. Presuming this is happening in branch "master" it would look like...

1. `git checkout master`
2. do all your removing/adding of old/new files
1. `git add .`
2. `git commit -am "replaced entire repo with better stuff"`
3. `git push`

#### OpenShift
If u are seeing `command not recognized` or other failures and having to ssh into something while trying to use git, that prolly means something is wrong. I have found OpenShift to be a little finicky, lately, actually, so I'll put my notes about it at [OpenShift](OpenShift)

### I Love You