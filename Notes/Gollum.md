### First Things First 
* Gollum is file-based. Which has several implications, but first and foremost it means that everything that appears in the browser is represented in the folder that is your gollum root. So if I set up my gollum like this: 

* `mkdir -p ~/git/gollum #makes a folder to store yr gollum -- this will be your gollum root` 
then...
* `cd ~/git/gollum # move into that space` 
then...
* `git init . # initialize a git repo here` 
then...
* `gollum # start the gollum engine/server` 
then...
* visit [http://localhost:4567](http://localhost:4567/) 
* you'll be thrown to [http://localhost:4567/create/Home](http://localhost:4567/create/Home) 
<a href="../media/images/create-home.png"><img src="../media/images/create-home.png" width="400" style="float:left;"/></a> 
* Do it! Make your "Home" article, save it, then go look at your gollum root and you'll see a new file there: Home.md 
<a href="../media/images/create-home-result.png"><img src="../media/images/create-home-result.png" width="400" style="float:left;"/></a> 
* If you had created a wiki node called "Away" your gollum root would look like... 

		`gollum/ 
			-->Away.md 
			-->Home.md`

* Likewise, if you then created a wiki node called "About/ThisThing" (note the slash will translate to a new directory) your gollum root would look like... 

		`gollum/ 
		-->About/ 
			-->ThisThing.md 
		-->Away.md 
		-->Home.md`

### git add . && git commit -am "my new thing" 

The most annoying thing about gollum is that the only way to see content immediately appear (i.e. the only way to live-edit) is to edit using the browser interface. That is, if you wanna use yr favorite Markdown* editor (e.g. [LightPaper](http://clockworkengine.com/lightpaper-mac/) or [Mou](http://mouapp.com/)) or even just your text editor and you add or edit a file, you have to `git add mynewfile.md` and then `git commit` same, and *then* restart the gollum server. I consistently forget this.

\* Of course Gollum supports more than Markdown, but the world appears to be going the Markdown way, so u might as well. 



## Sidebar
#### Intro
Now, it seems there's a lot of stuff that gollum supports that isn't obvious (or there by default). One of these is the Sidebar. For this, you need a file called "_Sidebar.ext" in your gollum directory (for now we'll start with just a single sidebar in the root of your gollum). Note that ".ext" here is prolly gonna be ".md" although it could be any filetype gollum supports.

#### Create _Sidebar.md
There are a number of ways to create this file (its contents can basically be anything). Some common ways:
* In your favorite Markdown or text editor, right-click in the empty space of your gollum file tree and create a new file and save it as "_Sidebar.md" in the root of yr gollum instance (maybe ~/git/gollum)
<a href="../media/images/create-sidebar.png">
	<img src="../media/images/create-sidebar.png" alt="" width="400" height="" border="0" />
</a>
* OR...
* in terminal type `echo "my sidebar contents" > ~/git/gollum/_Sidebar.md"`

...actually that's about it. There are other ways, but those are the two easiest.

## I love you. 
You know who you are.